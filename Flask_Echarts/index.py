# coding:utf-8
# 引入app配置
import json
import os
from datetime import datetime

from application import app, conn, cache  # 引入app
from flask import render_template, request  # 读取页面
from flask import send_from_directory
from pymysql import cursors


#pymysql.install_as_MySQLdb()

class DateEncoder(json.JSONEncoder ):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.__str__()
        return json.JSONEncoder.default(self, obj)


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


def connect_mysql(conn):
    # 建立操作游标
    #mysql_free_result();
    cursor = conn.cursor(cursors.SSCursor)
    return cursor


# 首页
@app.route('/')
def index():
    return render_template('index.html')


# 地点薪资图页面(地图)
# 图表形式参考链接：http://echarts.baidu.com/demo.html#map-china-dataRange
@app.route('/zwyx/dd_index')
def zwyx_dd_view():
    return render_template('zwyx_dd.html')


# 地点和薪资
@app.route('/zwyx/dd')
@cache.cached(timeout=60*60*24*7)
def show_zwyx_dd():
    # 建立链接游标
    cursor = connect_mysql(conn)
    # 初始化返回的字典
    returnDate = {}
    returnDate['status'] = 0
    # 查询地点和薪资的关系，职位总数，平均薪资
    sql = "select avg((zp_list.max_zwyx+zp_list.min_zwyx)/2) as avg_zwyx,zp_dd.province,zp_dd.id from zp_list inner join zp_dd on zp_dd.Id=zp_list.dd_id where province is not NULL and zp_list.min_zwyx!=0 group by province"
    # 执行sql语句
    cursor.execute(sql)
    # 取出所有结果集
    #dd_zwyx_list = cursor.fetchall()
    # 平均薪资
    avg_zwyx = {}
    # 总职位数
    count_zw = {}
    if cursor:
        # 循环遍历重新构建数据
        for value in cursor:
            # 取得地点名
            dd_name = value[1]
            # 判断平均薪资数据录入
            count_zw[dd_name] = {'name': dd_name, 'value': float(round(value[0], 2))}
        # 重新构建数据
        return_avg_zwyx = list(count_zw.values())
        # 数据汇总
        returnDate['avg_zwyx'] = return_avg_zwyx
        returnDate['status'] = 1
    # 关闭游标链接
    cursor.close()
    rv = json.dumps(returnDate)
    return rv


# 学历薪资图页面（柱状图）
# 参考链接:http://echarts.baidu.com/demo.html#bar-stack
@app.route('/zwyx/xl_index')
def zwyx_xl_view():
    return render_template('zwyx_xl.html')


# 学历和薪资
@app.route('/zwyx/xl')
@cache.cached(timeout=60*60*24*7)
def show_zwyx_xl():
    # 建立链接游标
    cursor = connect_mysql(conn)
    returnDate = {}
    returnDate['status'] = 0
    returnDate['data'] = {}
    # 查询地点和薪资的关系，职位总数，平均薪资
    sql = 'select count(zp_list.Id) as count_zw,avg((zp_list.max_zwyx+zp_list.min_zwyx)/2) as avg_zwyx,zp_xl.xl_name,max(max_zwyx),min(max_zwyx) from zp_list inner join zp_xl on zp_xl.Id=zp_list.xl_id where min_zwyx!=0 group by xl_id order by count_zw desc'
    # 执行sql语句
    cursor.execute(sql)
    # 取出所有结果集
    #xl_zwyx_list = cursor.fetchall()
    if cursor:
        returnDate['status'] = 1
        # 总职位数
        count_zw = []
        # 平均薪资
        avg_zw = []
        # 学历名
        xl_list = []
        # 最大薪资
        max_xz = []
        # 最小薪资
        min_xz = []
        # 循环遍历存入数据
        for item in cursor:
            count_zw.append(item[0])
            avg_zw.append(float(round(item[1], 2)))  # 精度保留2位小数
            xl_list.append(item[2])
            max_xz.append(int(item[3]))
            min_xz.append(int(item[4]))
        # 数据json化
        returnDate['count_zw'] = count_zw
        returnDate['avg_zw'] = avg_zw
        returnDate['xl_list'] = xl_list
        returnDate['max_xz'] = max_xz
        returnDate['min_xz'] = min_xz
    cursor.close()
    rv = json.dumps(returnDate)
    return rv


# 公司规模薪资图页面（饼图）
# 参考链接：http://echarts.baidu.com/demo.html#pie-roseType
@app.route('/zwyx/gsgm_index')
def zwyx_gsgm_view():
    return render_template('zwyx_gsgm.html')


# 公司规模和薪资（饼图）
@app.route('/zwyx/gsgm')
@cache.cached(timeout=60*60*24*7)
def show_zwyx_gsgm():
    # 建立链接游标
    cursor = connect_mysql(conn)
    returnDate = {}
    returnDate['status'] = 0
    # 查询地点和薪资的关系，职位总数，平均薪资
    sql = 'select count(zp_list.Id) as count_zw,avg((zp_list.max_zwyx+zp_list.min_zwyx)/2) as avg_zwyx,zp_gsgm.gsgm_name from zp_list inner join zp_gsgm on zp_gsgm.Id=zp_list.gsgm_id where min_zwyx!=0 group by gsgm_id order by count_zw desc'
    # 执行sql语句
    cursor.execute(sql)
    # 取出所有结果集
    #gsgm_zwyx_list = cursor.fetchall()
    if cursor:
        returnDate['status'] = 1
        # 总职位数
        count_zw = []
        # 平均薪资
        avg_zw = []
        # 公司规模名
        gsgm_list = []
        # 循环遍历存入数据
        for item in cursor:
            count_zw.append({'name': item[2], 'value': item[0]})
            avg_zw.append({'name': item[2], 'value': float(round(item[1], 2))})
            gsgm_list.append(item[2])
        returnDate['count_zw'] = count_zw
        returnDate['avg_zw'] = avg_zw
        returnDate['gsgm_list'] = gsgm_list
    cursor.close()
    rv = json.dumps(returnDate)
    return rv


# 公司性质薪资图页面（折线图）
# 参考链接：http://echarts.baidu.com/demo.html#line-marker
@app.route('/zwyx/gsxz_index')
def zwyx_gsxz_view():
    return render_template('zwyx_gsxz.html')


# 公司性质和薪资
@app.route('/zwyx/gsxz')
@cache.cached(timeout=60*60*24*7)
def show_zwyx_gsxz():
    # 建立链接游标
    cursor = connect_mysql(conn)
    returnDate = {}
    # 查询地点和薪资的关系，职位总数，平均薪资
    sql = 'select count(zp_list.Id) as count_zw,avg((zp_list.max_zwyx+zp_list.min_zwyx)/2) as avg_zwyx,zp_gsxz.gsxz_name,max(max_zwyx),min(min_zwyx) from zp_list inner join zp_gsxz on zp_gsxz.Id=zp_list.gsxz_id where min_zwyx!=0 group by gsxz_id order by count_zw desc'
    # 执行sql语句
    cursor.execute(sql)
    # 取出所有结果集
    #gsxz_zwyx_list = cursor.fetchall()
    if cursor:
        returnDate['status'] = 1
        # 总职位数
        count_zw = []
        # 平均薪资
        avg_zw = []
        # 公司规模名
        gsxz_list = []
        # 最大薪资
        max_xz = []
        # 最小薪资
        min_xz = []
        # 循环遍历存入数据
        for item in cursor:
            count_zw.append({'name': item[2], 'value': item[0]})
            avg_zw.append({'name': item[2], 'value': float(round(item[1], 2))})
            gsxz_list.append(item[2])
            max_xz.append(int(item[3]))
            min_xz.append(int(item[4]))
        # 数据json化
        returnDate['count_zw'] = count_zw
        returnDate['avg_zw'] = avg_zw
        returnDate['gsxz_list'] = gsxz_list
        returnDate['max_xz'] = max_xz
        returnDate['min_xz'] = min_xz
    cursor.close()
    rv = json.dumps(returnDate)
    return rv


# 经验薪资图页面（雷达图）
# 参考链接：http://echarts.baidu.com/demo.html#radar-custom
@app.route('/zwyx/jy_index')
def zwyx_jy_view():
    return render_template('zwyx_jy.html')


# 经验和薪资
@app.route('/zwyx/jy')
@cache.cached(timeout=60*60*24*7)
def show_zwyx_jy():
    # 建立链接游标
    cursor = connect_mysql(conn)
    returnDate = {}
    returnDate['status'] = 0
    sql = 'select count(zp_list.Id) as count_zw,avg((zp_list.max_zwyx+zp_list.min_zwyx)/2) as avg_zwyx,zp_jy.jy_name,max(max_zwyx),min(min_zwyx) from zp_list inner join zp_jy on zp_jy.Id=zp_list.jy_id where min_zwyx!=0 group by jy_id order by count_zw desc'
    # 执行sql语句
    cursor.execute(sql)
    # 取出所有结果集
    #jy_zwyx_list = cursor.fetchall()
    if cursor:
        # 总职位数
        count_zw = []
        # 平均薪资
        avg_zw = []
        # 经验分类名
        jy_list = []
        # 最大薪资
        max_xz = []
        # 最小薪资
        min_xz = []
        # 循环遍历存入数据
        returnDate['status'] = 1
        for item in cursor:
            count_zw.append(item[0])
            avg_zw.append(float(round(item[1], 2)))
            jy_list.append({'text': item[2]})
            max_xz.append(int(item[3]))
            min_xz.append(int(item[4]))
        # 数据json化
        returnDate['count_zw'] = count_zw
        returnDate['avg_zw'] = avg_zw
        returnDate['jy_list'] = jy_list
        returnDate['max_xz'] = max_xz
        returnDate['min_xz'] = min_xz
    cursor.close()
    rv = json.dumps(returnDate)
    return rv


# 职位名称薪资图页面（象形柱图）
# 参考链接：http://echarts.baidu.com/demo.html#pictorialBar-dotted
@app.route('/zwyx/zwmc_index')
def zwyx_zwmc_view():
    return render_template('zwyx_zwmc.html')


# 职位名称和薪资
@app.route('/zwyx/zwmc')
@cache.cached(timeout=60*60*24*7)
def show_zwyx_zwmc():
    returnDate={}
    # 建立链接游标
    cursor = connect_mysql(conn)
    returnDate = {}
    returnDate['status'] = 0
    sql = 'select count(zp_list.Id) as count_zw,avg((zp_list.max_zwyx+zp_list.min_zwyx)/2) as avg_zwyx,zp_zwlb.zwlb_name,max(max_zwyx),min(min_zwyx) from zp_list inner join zp_zwlb on zp_zwlb.Id=zp_list.zwlb_id where min_zwyx!=0 and max_zwyx!=0 and zwlb_name!="其他" group by zp_list.zwlb_id order by count(zwlb_id) desc limit 10'
    # 执行sql语句
    cursor.execute(sql)
    # 总职位数
    #count_zw = {}
    # 平均薪资
    #avg_zw = {}
    # 职位名
    zwmc_list = []
    # 最大薪资
    #max_xz = {}
    # 最小薪资
    #min_xz = {}
    #count_zw['其他'] = []
    #avg_zw['其他'] = []
    #max_xz['其他'] = 0
    #min_xz['其他'] = 0
    #zwmc_zwyx_list = cursor.fetchall()
    # 打开职位分类文本
    return_data = {}
    return_avg_zw = []
    if cursor:
        # 提取数据
        for row in cursor:
            item = row[2]
            return_data.setdefault(item, {}).setdefault('count_zw', []).append(row[0])
            return_data[item].setdefault('avg_zw', []).append(row[1])
            return_data[item].setdefault('count_zw', []).append(row[0])
            return_data[item]['max_xz'] = float(max(return_data[item].get('max_xz', 0), row[3]))
            return_data[item]['min_xz'] = float(max(return_data[item].get('min_xz', 0), row[4]))
                    # 如果判断职位列表里是否存在该项
            '''if item in zwmc_list:
                #存在则处理数据
                count_zw[item].append(row[0])
                avg_zw[item].append(row[1])
                max_xz[item] = float(max(max_xz[item], row[3]))
                min_xz[item] = float(min(min_xz[item], row[4]))
            else:
                # 不存在则添加数据
                zwmc_list.add(item.decode('utf-8'))
                count_zw[item]=[]
                count_zw[item].append(row[0])
                avg_zw[item]=[]
                avg_zw[item].append(row[1])
                max_xz[item] = float(row[3])
                min_xz[item] = float(row[4])'''
        return_count_zw = []
        return_max_xz=[]
        return_min_xz = []
        for value in return_data:
            return_count_zw.append(sum(return_data[value]['count_zw']))
            avg_num_list = return_data[value]['avg_zw']
            return_avg_zw.append(float(round(sum(avg_num_list) / len(avg_num_list), 2)))
            zwmc_list.append(value)
            return_max_xz.append(return_data[value]['max_xz'])
            return_min_xz.append(return_data[value]['min_xz'])


        # 重新构建数据
        #return_max_xz = list(max_xz.values())
        #return_min_xz = list(min_xz.values())
        # json数据
        returnDate['status'] = 1
        returnDate['count_zw'] = return_count_zw
        returnDate['avg_zw'] = return_avg_zw
        returnDate['zwmc_list'] = list(zwmc_list)
        returnDate['max_xz'] = return_max_xz
        returnDate['min_xz'] = return_min_xz
    else:
        returnDate['status'] = 0
        cursor.close()
    rv = json.dumps(returnDate)
    return rv


# Top10最稀缺职位页面（象形柱图）
# 参考链接：http://echarts.baidu.com/demo.html#pictorialBar-dotted
@app.route('/zwyx/zwmc_last_index')
def zwyx_zwmc_last_view():
    return render_template('zwyx_zwmc_last.html')


# 职位名称和薪资
@app.route('/zwyx/zwmc_last')
@cache.cached(timeout=60 * 60 * 24 * 7)
def show_zwyx_zwmc_last():
    returnDate = {}
    # 建立链接游标
    cursor = connect_mysql(conn)
    returnDate = {}
    returnDate['status'] = 0
    sql = 'select count(zp_list.Id) as count_zw,avg((zp_list.max_zwyx+zp_list.min_zwyx)/2) as avg_zwyx,zp_zwlb.zwlb_name,max(max_zwyx),min(min_zwyx) from zp_list inner join zp_zwlb on zp_zwlb.Id=zp_list.zwlb_id where min_zwyx!=0 and max_zwyx!=0 group by zp_list.zwlb_id order by count(zwlb_id) asc limit 10'
    # 执行sql语句
    cursor.execute(sql)
    # 总职位数
    # count_zw = {}
    # 平均薪资
    # avg_zw = {}
    # 职位名
    zwmc_list = []
    # 最大薪资
    # max_xz = {}
    # 最小薪资
    # min_xz = {}
    # count_zw['其他'] = []
    # avg_zw['其他'] = []
    # max_xz['其他'] = 0
    # min_xz['其他'] = 0
    # zwmc_zwyx_list = cursor.fetchall()
    # 打开职位分类文本
    return_data = {}
    return_avg_zw = []
    if cursor:
        # 提取数据
        for row in cursor:
            item = row[2]
            return_data.setdefault(item, {}).setdefault('count_zw', []).append(row[0])
            return_data[item].setdefault('avg_zw', []).append(row[1])
            return_data[item].setdefault('count_zw', []).append(row[0])
            return_data[item]['max_xz'] = float(max(return_data[item].get('max_xz', 0), row[3]))
            return_data[item]['min_xz'] = float(max(return_data[item].get('min_xz', 0), row[4]))
        return_count_zw = []
        return_max_xz = []
        return_min_xz = []
        for value in return_data:
            return_count_zw.append(sum(return_data[value]['count_zw']))
            avg_num_list = return_data[value]['avg_zw']
            return_avg_zw.append(float(round(sum(avg_num_list) / len(avg_num_list), 2)))
            zwmc_list.append(value)
            return_max_xz.append(return_data[value]['max_xz'])
            return_min_xz.append(return_data[value]['min_xz'])

        # 重新构建数据
        # return_max_xz = list(max_xz.values())
        # return_min_xz = list(min_xz.values())
        # json数据
        returnDate['status'] = 1
        returnDate['count_zw'] = return_count_zw
        returnDate['avg_zw'] = return_avg_zw
        returnDate['zwmc_list'] = list(zwmc_list)
        returnDate['max_xz'] = return_max_xz
        returnDate['min_xz'] = return_min_xz
    else:
        returnDate['status'] = 0
        cursor.close()
    rv = json.dumps(returnDate)
    return rv

# 公司数和地点关系（散点图）
# 参考链接：http://echarts.baidu.com/demo.html#scatter-map-brush
@app.route('/dd/gsmc_index')
def dd_gsmc_view():
    return render_template('dd_gsmc.html')


# 公司数和地点
@app.route('/dd/gsmc')
@cache.cached(timeout=60*60*24*7)
def show_dd_gsmc():
    # 建立链接游标
    cursor = connect_mysql(conn)
    returnDate = {}
    returnDate['status'] = 0
    sql = 'select count(distinct zp_list.gsmc_id) as count_gs,zp_dd.dd_name from zp_list inner join zp_dd on zp_dd.Id=zp_list.dd_id group by zp_list.dd_id'
    # 执行sql语句
    cursor.execute(sql)
    # 公司数
    count_gs = []
    # 城市经纬度
    geoCoordMap={}
    if cursor:
        returnDate['status'] = 1
        for item in cursor:
            count_gs.append({'name': item[1], 'value': item[0]})
        returnDate['count_gs'] = count_gs
        sql_dd='select dd_name,format(pointx,2),format(pointy,2) from zp_dd where pointx>100 and pointx<999;'
        cursor.execute(sql_dd)
        for item1 in cursor:
            geoCoordMap[item1[0]]=[item1[1],item1[2]]
        returnDate['geoCoordMap'] = geoCoordMap
    else:
        returnDate['status'] = 0
    cursor.close()
    rv = json.dumps(returnDate)
    return rv


# 职位数和地点页面（热力图）
# 参考链接：http://echarts.baidu.com/demo.html#heatmap-map
@app.route('/dd/zwmc_index')
def dd_zwmc_view():
    return render_template('dd_zwmc.html')


# 职位名称和地点
@app.route('/dd/zwmc')
@cache.cached(timeout=60*60*24*7)
def show_dd_zwmc():
    # 建立链接游标
    cursor = connect_mysql(conn)
    returnDate = {}
    returnDate['status'] = 0
    sql = 'select count(distinct zp_list.zwmc_id) as count_zw,zp_dd.dd_name from zp_list inner join zp_dd on zp_dd.Id=zp_list.dd_id group by zp_list.dd_id'
    # 执行sql语句
    cursor.execute(sql)
    # 职位数
    count_zw = []
    # 城市经纬度
    geoCoordMap={}
    if cursor:
        returnDate['status'] = 1
        for item in cursor:
            count_zw.append({'name': item[1], 'value': item[0]})
        returnDate['count_zw'] = count_zw
        sql_dd='select dd_name,format(pointx,2),format(pointy,2) from zp_dd where pointx>100 and pointx<999;'
        cursor.execute(sql_dd)
        for item1 in cursor:
            geoCoordMap[item1[0]]=[item1[1],item1[2]]
        returnDate['geoCoordMap'] = geoCoordMap
    else:
        returnDate['status'] = 0
    cursor.close()
    rv = json.dumps(returnDate)
    return rv


# 职位类型相关关系（综合图）
# 参考链接：http://echarts.baidu.com/demo.html#watermark
@app.route('/dd/type_index')
def dd_type_view():
    return render_template('dd_type.html')


@app.route('/zwmc/search')
def zwmc_search():
    # 建立链接游标
    cursor = connect_mysql(conn)
    # 省份
    provinceList = []
    province_sql = 'select province from zp_dd where province!="" and pointx<200 group by province'
    cursor.execute(province_sql)
    for item in cursor:
        provinceList.append(item[0])
    # 城市
    cityList = []
    city_sql = 'select id,dd_name from zp_dd'
    cursor.execute(city_sql)
    for item in cursor:
        cityList.append({'id': item[0], 'dd_name': item[1]})
    # 公司性质
    gsxzList = []
    gsxz_sql = 'select * from zp_gsxz'
    cursor.execute(gsxz_sql)
    for item in cursor:
        gsxzList.append({'id': item[0], 'gsxz_name': item[1]})
    # 公司规模
    gsgmList = []
    gsgm_sql = 'select * from zp_gsgm'
    cursor.execute(gsgm_sql)
    for item in cursor:
        gsgmList.append({'id': item[0], 'gsgm_name': item[1]})
    # 学历
    xlList = []
    xl_sql = 'select * from zp_xl'
    cursor.execute(xl_sql)
    for item in cursor:
        xlList.append({'id': item[0], 'xl_name': item[1]})
    # 工作经验
    gzjyList = []
    gzjy_sql = 'select * from zp_gzjy'
    cursor.execute(gzjy_sql)
    for item in cursor:
        gzjyList.append({'id': item[0], 'gzjy_name': item[1]})
    # 公司行业
    gshyList = []
    gshy_sql = 'select * from zp_gshy'
    cursor.execute(gshy_sql)
    for item in cursor:
        gshyList.append({'id': item[0], 'gshy_name': item[1]})
    # 大分类
    zwlbBigList = []
    zwlb_big_sql = 'select * from zp_zwlb_big'
    cursor.execute(zwlb_big_sql)
    for item in cursor:
        zwlbBigList.append({'id': item[0], 'zwlb_big_name': item[1]})
    # 小分类
    zwlbList = []
    zwlb_sql = 'select * from zp_zwlb'
    cursor.execute(zwlb_sql)
    for item in cursor:
        zwlbList.append({'id': item[0], 'zwlb_name': item[1]})
    cursor.close()
    return render_template('zw_search.html', provinces=provinceList, cities=cityList, gsxz=gsxzList, gsgm=gsgmList,
                           xl=xlList, gzjy=gzjyList, gshy=gshyList, zwlb_big=zwlbBigList, zwlb=zwlbList)

# 获取省份对应城市信息
@cache.memoize(timeout=60 * 60 * 24 * 7)
@app.route('/get/city/list')
def city_list():
    # 省份
    # 建立链接游标
    cursor = connect_mysql(conn)
    cityList = []
    province_sql = 'select id,dd_name from zp_dd where province=%s'
    cursor.execute(province_sql,(request.args.get('province','')))
    for item in cursor:
        cityList.append({'id': item[0], 'dd_name': item[1]})
    cursor.close()
    return json.dumps(cityList)

@cache.memoize(timeout=60 * 60 * 24 * 7)
@app.route('/get/zwlb/list')
def zwlb_list():
    # 职位分类
    # 建立链接游标
    cursor = connect_mysql(conn)
    zwlbList = []
    zwlb_sql = 'select id,zwlb_name from zp_zwlb where zwlb_big_id=%s'
    cursor.execute(zwlb_sql, (request.args.get('zwlb','')))
    for item in cursor:
        zwlbList.append({'id': item[0], 'zwlb_name': item[1]})
    cursor.close()
    return json.dumps(zwlbList)


@cache.memoize(timeout=60 * 60 * 24 * 7)
@app.route('/zw/list')
def zw_list():
    # 建立链接游标
    cursor = connect_mysql(conn)
    page = request.args.get('page', '1')
    limit = request.args.get('limit', '10')
    province = request.args.get('province','')
    dd_id=int(request.args.get('city','0'))
    gsxz_id=int(request.args.get('gsxz','0'))
    gsgm_id=int(request.args.get('gsgm','0'))
    xl_id=int(request.args.get('xl','0'))
    gzjy_id=int(request.args.get('gzjy','0'))
    gshy_id=int(request.args.get('gshy','0'))
    zwlb_big_id=int(request.args.get('zwlb_big','0'))
    zwlb_id=int(request.args.get('zwlb','0'))
    zw_list_sql='select zp_list.id,dd_name,gsxz_name,gsgm_name,xl_name,gzjy_name,gshy_name,zwlb_name,min_zwyx,max_zwyx,zwmc_name,gsmc_name from zp_list inner join zp_dd on zp_dd.id=zp_list.dd_id inner join zp_gsgm on zp_gsgm.id=zp_list.gsgm_id inner join zp_gsxz on zp_gsxz.id=zp_list.gsxz_id inner join zp_xl on zp_xl.id=zp_list.xl_id inner join zp_gzjy on zp_gzjy.id=zp_list.gzjy_id inner join zp_gshy on zp_gshy.id=zp_list.gshy_id inner join zp_zwlb on zp_zwlb.id=zp_list.zwlb_id inner join zp_zwmc on zp_zwmc.id=zp_list.zwmc_id inner join zp_gsmc on zp_gsmc.id=zp_list.gsmc_id '
    zw_list_count_sql = 'select count(*) from zp_list inner join zp_dd on zp_dd.id=zp_list.dd_id inner join zp_gsgm on zp_gsgm.id=zp_list.gsgm_id inner join zp_gsxz on zp_gsxz.id=zp_list.gsxz_id inner join zp_xl on zp_xl.id=zp_list.xl_id inner join zp_gzjy on zp_gzjy.id=zp_list.gzjy_id inner join zp_gshy on zp_gshy.id=zp_list.gshy_id inner join zp_zwlb on zp_zwlb.id=zp_list.zwlb_id inner join zp_zwmc on zp_zwmc.id=zp_list.zwmc_id inner join zp_gsmc on zp_gsmc.id=zp_list.gsmc_id '
    sql_list=[]
    if dd_id!=0:
        sql_list.append('dd_id='+str(dd_id))
    elif dd_id == 0 and province!='':
        sql_list.append('province="'+province+'"')
    if gsxz_id != 0:
        sql_list.append('gsxz_id=' + str(gsxz_id))
    if gsgm_id != 0:
        sql_list.append('gsgm_id=' + str(gsgm_id))
    if xl_id != 0:
        sql_list.append('xl_id=' + str(xl_id))
    if gzjy_id != 0:
        sql_list.append('gzjy_id=' + str(gzjy_id))
    if gshy_id != 0:
        sql_list.append('gshy_id=' + str(gshy_id))
    if zwlb_id != 0:
        sql_list.append('zwlb_id=' + str(zwlb_id))
    elif zwlb_id == 0 and zwlb_big_id!=0:
        sql_list.append('zp_list.zwlb_big_id=' + str(zwlb_big_id))
    if len(sql_list)>0:
        zw_list_sql+='where '+' and '.join(sql_list)
        zw_list_count_sql += 'where ' + ' and '.join(sql_list)
    zw_list_sql+=' order by id desc limit '+str((int(page)-1)*int(limit))+','+limit
    zwList=[]
    # print(zw_list_sql)
    cursor.execute(zw_list_sql)
    zwCount=0
    for item in cursor:
        # select zp_list.id,dd_name,gsxz_name,gsgm_name,xl_name,gzjy_name,gshy_name,zwlb_name,min_zwyx,max_zwyx,count(*) from zp_list
        zwItem={'id': item[0], 'dd_name': item[1], 'gsxz_name': item[2],'gsgm_name':item[3],'xl_name':item[4],'gzjy_name':item[5],'gshy_name':item[6],'zwlb_name':item[7],'min_zwyx':item[8],'max_zwyx':item[9],'zwmc_name':item[10],'gsmc_name':item[11]}
        zwList.append(zwItem)
    cursor.execute(zw_list_count_sql)
    for item in cursor:
        zwCount = item[0]
    cursor.close()
    return json.dumps({'code':0,'msg':'','count':zwCount,'data':zwList})

# 入口
if __name__ == '__main__':
    # 调试模式
    # app.debug = True
    # 外部可访问的服务器
    app.run(host='0.0.0.0')
    conn.close()
