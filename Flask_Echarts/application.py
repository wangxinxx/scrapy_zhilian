# coding:utf-8
import pymysql
from flask import Flask  # 默认
from flask_cache import Cache
from pymysql import cursors

# app入口定义
app = Flask(__name__)
#链接数据库
conn = pymysql.connect(host='127.0.0.1', cursorclass=cursors.SSCursor, user='root', passwd='FanTan879', db='zp',
                       charset='utf8mb4')
conn.encoding='utf8'

cache = Cache()
config = {
  'CACHE_TYPE': 'redis',
  'CACHE_REDIS_HOST': '47.104.82.16',
  'CACHE_REDIS_PORT': 6379,
  'CACHE_REDIS_DB': '1',
  'CACHE_REDIS_PASSWORD': 'FanTan879425'
}
app.config.from_object(config)
cache.init_app(app,config)
